/*
 * grunt-kraken_stitchfix
 * 
 *
 * Copyright (c) 2016 
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Configuration to be run (and then tested).
    kraken_stitchfix: {
        options: {
            key: '',
            secret: '',
        },
        dynamic: {
            files: [{
                expand: true,
                cwd: 'images/',
                src: ['**/*.{png,jpg,jpeg,gif}'],
                dest: 'imagesKraked/'
            }]
        }
    }
  });
 
  grunt.loadTasks('tasks');
  grunt.registerTask('default', ['kraken_stitchfix']);

};
