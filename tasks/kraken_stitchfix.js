
/*
 * grunt-kraken_stitchfix
 * 
 *
 * Copyright (c) 2016 
 * Licensed under the MIT license.
 */
'use strict';

var fs = require('fs'),
  os = require('os'),
  async = require('async'),
  Kraken = require('kraken'),
  isGIF = require('is-gif'),
  isPNG = require('is-png'),
  isJPG = require('is-jpg'),
  request = require('request'),
  chalk = require('chalk'),
  pretty = require('pretty-bytes');


module.exports = function(grunt) {
  grunt.registerMultiTask('kraken_stitchfix', 'Compress images using Kraken', function() {

    var done = this.async(),
      files = this.files,
      options = this.options({
        lossy: false
      });

    var total = {
      bytes: 0,
      kraked: 0,
      files: 0
    };

    async.forEachLimit(files, os.cpus().length, function(file, next) {
      var isSupported = !isPNG(file.src[0]) || !isJPG(file.src[0]);
  
      if (!isSupported) {
        grunt.warn('Skipping unsupported image ' + file.src[0]);
        return next();
      }

      var kraken = new Kraken({
        api_key: options.key,
        api_secret: options.secret
      });

      var opts = {
        file: file.src[0],
        lossy: isJPG(file.src[0]),
        wait: true,
        preserve_meta: ["profile"]
      };

      function directoryPath(path){ 
        var str = path; 
        var res = /.+\//.exec(str);
        return res[0];
      }

      function checkDirectorySync(directory) {  

        try {
          fs.statSync(directory);
           
        } catch(e) {
          console.log(chalk.green('✔ ')+ 'created destination directory:'+ chalk.magenta(directory));
          fs.mkdirSync(directory);
        }
      }

      kraken.upload(opts, function(data) {
        if (!data.success) {
          grunt.warn('Error in file ' + file.src[0] + ': ' + data.message || data.error);
          return next();
        }

        var originalSize = data.original_size,
          krakedSize = data.kraked_size,
          savings = data.saved_bytes;

        var percent = (((savings) * 100) / originalSize).toFixed(2),
          savedMsg = 'saved ' + pretty(savings) + ' - ' + percent + '%',
          msg = savings > 0 ? savedMsg : 'already optimized';

        total.bytes += originalSize;
        total.kraked += krakedSize;
        total.files++;

        

        var directory = directoryPath(file.dest);
       
        checkDirectorySync(directory);
        request(data.kraked_url, function(err) {

          if (err) {
            grunt.warn(err + ' in file ' + file.src[0]);
            return next();
          }

          grunt.log.writeln(chalk.green('✔ ') + file.src[0] + chalk.gray(' (' + msg + ')'));
          process.nextTick(next);
      
        }).pipe(fs.createWriteStream(file.dest));
      });
    }, function(err) {
      if (err) {
        grunt.warn(err);
      }

      var percent = (((total.bytes - total.kraked) * 100) / total.bytes).toFixed(2);
      var savings = total.bytes - total.kraked;
      var msg = 'All done. Kraked ' + total.files + ' image';

      msg += total.files === 1 ? '' : 's';
      msg += chalk.gray(' (saved ' + pretty(savings) + ' - ' + percent + '%)');

      grunt.log.writeln(msg);
      done();

    });
  });
};